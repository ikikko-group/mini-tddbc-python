# テスト実行方法

```
# poetryのインストール
curl -sSL https://install.python-poetry.org | python3 -

# 他にも、pipやHomebrew経由でインストールする方法もあり
# pip install poetry
# brew install poetry


# 依存ライブラリのインストール
poetry install


# pytestの実行
poetry run pytest -v
```