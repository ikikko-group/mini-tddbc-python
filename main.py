def hello():
  return "Hello, Replit!"


if __name__ == '__main__':
  print(hello())


def myers(a, b, c):

  if a == b == c:
    return "正三角形です"

  if is_二等辺三角形(a, b, c):
    return "二等辺三角形です"

  return "不等辺三角形です"


def is_二等辺三角形(a, b, c):
  return a == b or b == c or a == c
